const containerPokemon = document.querySelector('.footer__othersPokemons');
const containerInfo = document.querySelectorAll('.main__info');
const containerTitle = document.querySelector('.main__titleContainer');
const containerMain = document.querySelector('.main');

loadEventListener();
function loadEventListener() {
    // Select pokemon 
    containerPokemon.addEventListener('click', selectPokemon);
}

function selectPokemon(e) {
    if(e.target.classList.contains('footer__pokemonImg')) {
        const pokemonSelected = e.target;
        ReadDataPokemon(pokemonSelected);
    }
}

function ReadDataPokemon(pokemon) {

    // Create object with data pokemon
    const infoPokemon = {
        image: pokemon.src,
        name: pokemon.getAttribute('data-name'),
        level: pokemon.getAttribute('data-level'),
        number: pokemon.getAttribute('data-number'),
        type: pokemon.getAttribute('data-type'),
        hability: pokemon.getAttribute('data-hability'),
        height: pokemon.getAttribute('data-height'),
        weight: pokemon.getAttribute('data-weight'),
    }
    printPokemon(infoPokemon);
}

function printPokemon(pokemon) {
    // Clean HTML
    cleanHTML();

    const { image, name, level, number, type, hability, height, weight } = pokemon;

    // Generate HTML
    const pokemonHeaderHTML = document.createElement('div');
    pokemonHeaderHTML.classList.add('main__titleContainer');
    pokemonHeaderHTML.innerHTML = `
        <div class="main__titleBox">
            <i class="fas fa-brain main__icon"></i>
            <h2 class="main__namePokemon">${name}</h2>
        </div>
        <img src="${image}" alt="" class="main__pokemon">
    `;
    const pokemonInfoHTML = document.createElement('div');
    pokemonInfoHTML.classList.add('main__infoBox');
    pokemonInfoHTML.innerHTML =`
        <div class="main__info">                 
            <p>N&deg;</p>                       
            <p>${number}</p>                        
        </div>
        <div class="main__info">
            <p>LEVEL</p>
            <p>${level}</p>
        </div>
        <div class="main__info">
            <p>TYPE</p>
            <p>${type}</p>
        </div>
        <div class="main__info">
            <p>HABILITY</p>
            <p>${hability}</p>
        </div>
        <div class="main__info">
            <p>HEIGHT</p>
            <p>${height}</p>
        </div>
        <div class="main__info">
            <p>WEIGHT</p>
            <p>${weight}</p>
        </div> 
    `;

    containerMain.append(pokemonHeaderHTML);
    containerMain.append(pokemonInfoHTML);
}

function cleanHTML() {
    while(containerMain.firstChild) {
        containerMain.removeChild(containerMain.firstChild)
    }
}